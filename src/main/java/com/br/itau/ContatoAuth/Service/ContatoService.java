package com.br.itau.ContatoAuth.Service;

import com.br.itau.ContatoAuth.DTO.ContatoDTO;
import com.br.itau.ContatoAuth.Model.Contato;
import com.br.itau.ContatoAuth.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContatoService
{
    @Autowired
    ContatoRepository contatoRepository;

    public ContatoDTO criar(ContatoDTO contatoDTO, Contato contato)
    {
        Contato contato = new Contato();
        contato.setEmail(contatoDTO.getEmail());
        contato.setName(contatoDTO.getName());
        contato.setTelephone(contatoDTO.getTelephone());
        contato.setContato(contato.getName());

        return  new ContatoDTO(contatoRepository.save(contato));
    }

    public List<ContatoDTO> buscar(Contato contato)
    {
        List<Contato> contatos = contatoRepository.findByContato(contato.getName());
        List<ContatoDTO> contatosDTO = new ArrayList<>();
        for(Contato contato : contatos)
        {
            contatosDTO.add(new ContatoDTO(contato));
        }

        return contatosDTO;
    }
}
