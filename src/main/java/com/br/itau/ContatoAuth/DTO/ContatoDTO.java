package com.br.itau.ContatoAuth.DTO;

import com.br.itau.ContatoAuth.Model.Contato;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ContatoDTO
{
    @NotNull(message =  "Nome não pode ser nulo")
    @NotBlank(message =  "Nome não pode ser branco")
    private String name;

    @NotNull(message =  "Email não pode ser nulo")
    @NotBlank(message =  "Email não pode ser branco")
    @Email(message = "Email inválido")
    private String email;

    @NotNull(message =  "Telefone não pode ser nulo")
    @NotBlank(message =  "Telefone não pode ser branco")
    private String telephone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return getTelephone();
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public ContatoDTO() {
    }

    public ContatoDTO(Contato contato)
    {
        this.setEmail(contato.getEmail());
        this.setName(contato.getName());
        this.setTelephone(contato.getTelephone());
    }
}
