package com.br.itau.ContatoAuth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContatoAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContatoAuthApplication.class, args);
	}

}
