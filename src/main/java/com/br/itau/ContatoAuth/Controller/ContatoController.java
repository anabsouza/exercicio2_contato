package com.br.itau.ContatoAuth.Controller;


import com.br.itau.ContatoAuth.Model.Contato;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contato")
public class ContatoController
{
    @GetMapping()
    public Contato buscarInformacoesContatoAutenticado(@AuthenticationPrincipal Contato contatoAutenticado) {
        return contatoAutenticado;
    }

}
