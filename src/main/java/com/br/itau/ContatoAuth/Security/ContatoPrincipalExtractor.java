package com.br.itau.ContatoAuth.Security;

import com.br.itau.ContatoAuth.Model.Contato;

import java.util.Map;

public class ContatoPrincipalExtractor implements ContatoPrincipalExtractor {
    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        Contato contato = new Contato(telephone);

        contato.setId((Integer) map.get("id"));
        contato.setName((String) map.get("name"));
        contato.setTelephone((String) map.get("telephone"));

        return contato;
    }



